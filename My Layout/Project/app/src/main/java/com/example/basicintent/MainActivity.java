package com.example.basicintent;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void SumNumber(View view) {
        EditText no1 = findViewById(R.id.txtNum1);
        EditText no2 = findViewById(R.id.txtNum2);
        String num1 = no1.getText().toString();
        String num2 = no2.getText().toString();

        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("num1",num1);
        intent.putExtra("num2",num2);
        startActivityForResult(intent, 111);
    }
}