package com.example.activityforresult;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
public class SecondActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Intent i = getIntent();
        String inputString = i.getStringExtra("yourname");
        String yourPhone = i.getStringExtra("yourphone");
        TextView view = (TextView) findViewById(R.id.tvShow);
        view.setText("Hi, " + inputString + "\nYour age is " + yourPhone.toString());
    }
    public void CallBackMainActivity(View view) {
        Intent intent = new Intent();
        EditText editText= (EditText) findViewById(R.id.etmsg);
        String string = editText.getText().toString();
        intent.putExtra("returnkey", string);
        setResult(RESULT_OK, intent);
        super.finish();
    }
    public void CallCancel(View view) {
        Intent intent = new Intent();
        intent.putExtra("returnkey", "Cancel");
        setResult(RESULT_CANCELED, intent);
        super.finish();
    }
}